<?php
namespace TkachInc\ChildProcesses;

use TkachInc\ChildProcesses\Handlers\EvChildProcessHandler;
use TkachInc\ChildProcesses\Handlers\EventChildProcessHandler;
use TkachInc\ChildProcesses\Handlers\LoopChildProcessHandler;

/**
 * @author maxim
 */
class ChildProcesses
{

	const TYPE_ERROR = 'ERROR';

	const TYPE_THROWABLE = 'THROWABLE';

	const TYPE_LAST_ERROR = 'LAST_ERROR';

	/**
	 * @var LoopChildProcessHandler
	 */
	protected $handler;

	protected $eventHandler;

	/**
	 * @var array Array processes (key - pid, value - process object)
	 */
	protected $processes = [];

	/**
	 * ChildProcesses constructor. Initialization process handler
	 * @param EventHandler $eventHandler
	 */
	public function __construct(EventHandler $eventHandler)
	{
		if (extension_loaded('Ev')) {
			$this->handler = new EvChildProcessHandler($eventHandler);
		} elseif (extension_loaded('Event')) {
			$this->handler = new EventChildProcessHandler($eventHandler);
		} else {
			$this->handler = new LoopChildProcessHandler($eventHandler);
		}

		$shutdown = function () {
			$this->killAll();
		};

		$this->getHandlerEmitter()->on(EventHandler::EVENT_STATUS_CLOSE, $shutdown);
		$this->getHandlerEmitter()->on(EventHandler::EVENT_ERROR_CATCH, $shutdown);
	}

	public function getHandlerEmitter() {
		return $this->handler->getEventEmitter();
	}

	/**
	 * @param string $command Shell command
	 * @param null $data
	 * @param resource $stdin
	 * @param resource $stdout
	 * @param resource $strerr
	 *
	 * @return null|ChildProcess
	 */
	public function add($command, $data = null, $stdin = STDIN, $stdout = STDOUT, $strerr = STDERR)
	{
		try {
			$process = ChildProcess::run($command, $data, $stdin, $stdout, $strerr);

			$this->processes[$process->getPID()] = $process;

			return $process;
		} catch (\Exception $e) {
			return null;
		}
	}

	/**
	 * @param ChildProcess $process
	 *
	 * @return $this
	 */
	public function addProcessInstance(ChildProcess $process)
	{
		$this->processes[$process->getPID()] = $process;

		return $this;
	}

	/**
	 * Check daemon process
	 */
	public function daemon()
	{
		while (true) {
			/** @var ChildProcess $process */
			foreach ($this->processes as $process) {
				$process->start();
			}

			stream_set_blocking(STDIN, false);
			$command = fgetc(STDIN);
			switch ($command) {
				case 'q':
				case 'e':
					return;
					break;
			}
			usleep(100000);
		}
	}

	/**
	 * @param $pid
	 *
	 * @return null|ChildProcess
	 */
	public function getProcess($pid)
	{
		return $this->processes[$pid]??null;
	}

	/**
	 * @return array
	 */
	public function getProcesses()
	{
		return $this->processes;
	}

	/**
	 * @return LoopChildProcessHandler
	 */
	public function getHandler()
	{
		return $this->handler;
	}

	/**
	 * @return array List fail commands
	 */
	public function check()
	{
		$fails = $this->handler->check($this->processes);

		return $fails;
	}

	/**
	 * Kill all processes in group
	 *
	 * @param int  $signal
	 * @param bool $force
	 */
	public function killAll($signal = SIGTERM, $force = false)
	{
		/** @var ChildProcess $process */
		foreach ($this->processes as $key => $process) {
			if (!$process->isKilling() || $force) {
				$process->kill($signal);
			}
		}
	}

	/**
	 * @param $pid
	 * @param $signal
	 *
	 * @return bool
	 */
	public function kill($pid, $signal = SIGTERM)
	{
		$process = $this->getProcess($pid);
		if ($process) {
			$process->kill($signal);

			return true;
		}

		return false;
	}
}
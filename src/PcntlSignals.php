<?php
namespace TkachInc\ChildProcesses;

/**
 * @author maxim
 */
class PcntlSignals
{

	protected static $workers = [];

	/**
	 * @param callable $function
	 * @param          $signal
	 */
	public static function add(Callable $function, $signal = SIGTERM)
	{
		static::$workers[$signal][] = $function;
	}

	/**
	 * Clear all callback
	 */
	public static function clear()
	{
		$signals = [];
		foreach (static::$workers as $signal => $callbacks) {
			$signals[$signal] = 1;
		}
		static::$workers = [];

		foreach ($signals as $signal => $v) {
			static::setDefault($signal);
		}
	}

	/**
	 * @param $signal
	 */
	public static function setDefault($signal)
	{
		pcntl_signal($signal, SIG_DFL);
	}

	/**
	 * @param $signal
	 */
	public static function setIgnore($signal)
	{
		pcntl_signal($signal, SIG_IGN);
	}

	/**
	 * Registration Closure in pcntl_signal
	 */
	public static function registration()
	{
		foreach (static::$workers as $signal => $callbacks) {
			pcntl_signal($signal, function () use ($signal, $callbacks) {
				static::work($callbacks, $signal);
			});
		}
	}

	/**
	 * @param $signal
	 */
	public static function call($signal)
	{
		if (isset(static::$workers[$signal])) {
			static::work(static::$workers[$signal], $signal);
		}
	}

	/**
	 * @param array $callbacks
	 * @param       $signal
	 */
	protected static function work(Array $callbacks, $signal)
	{
		foreach ($callbacks as $function) {
			if (is_callable($function)) {
				call_user_func_array($function, [$signal]);
			}
		}
	}

	/**
	 * @param bool $registration
	 *
	 * @return bool
	 */
	public static function dispatch($registration = true)
	{
		if ($registration) {
			static::registration();
		}

		return pcntl_signal_dispatch();
	}
}
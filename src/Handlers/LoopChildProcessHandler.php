<?php
namespace TkachInc\ChildProcesses\Handlers;

use TkachInc\ChildProcesses\ChildProcess;
use TkachInc\ChildProcesses\EventHandler;
use TkachInc\ChildProcesses\PcntlSignals;

/**
 * @author maxim
 */
class LoopChildProcessHandler extends AbstractProcessHandler
{

	private $keepRun = true;

	public function init()
	{
		$this->eventHandler->on(EventHandler::EVENT_CHECK_COMPLETE, function () {
			$this->keepRun = false;
		});
	}

	public function check(&$processes)
	{
		PcntlSignals::clear();

		$this->eventHandler->emit(EventHandler::EVENT_CHECK_INIT, []);
		PcntlSignals::add(function () {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGTERM]);
		}, SIGTERM);

		PcntlSignals::add(function () {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGINT]);
		}, SIGINT);

		PcntlSignals::add(function () {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGHUP]);
		}, SIGHUP);

		while ($this->keepRun) {
			PcntlSignals::dispatch();

			/** @var ChildProcess $process */
			foreach ($processes as $key => $process) {
				$status = $process->getStatus();

				if (!$status['running']) {
					$this->eventWorkerFinish($status['exitcode'], $process->getPID(), $processes);
				}
			}

			usleep(100000);
		}

		return $this->fails;
	}
}
<?php
namespace TkachInc\ChildProcesses\Handlers;

/**
 * @author maxim
 */
interface InterfaceProcessHandler
{

	public function check(&$processes);
	public function init();
}
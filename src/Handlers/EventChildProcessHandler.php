<?php
namespace TkachInc\ChildProcesses\Handlers;

use Event;
use EventBase;
use TkachInc\ChildProcesses\ChildProcess;
use TkachInc\ChildProcesses\EventHandler;

/**
 * @author maxim
 */
class EventChildProcessHandler extends AbstractProcessHandler
{

	protected $base;

	public function init()
	{
		$this->base = new EventBase();
		$this->eventHandler->on(EventHandler::EVENT_CHECK_COMPLETE, function () {
			$this->base->exit();
		});
	}

	public function check(&$processes)
	{
		$this->eventHandler->emit(EventHandler::EVENT_CHECK_INIT, []);
		$sigtermEvent = Event::signal($this->base, SIGTERM, function ($no, $c) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGTERM]);
		});
		$sigtermEvent->add();
		$sigintEvent = Event::signal($this->base, SIGINT, function ($no, $c) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGINT]);
		});
		$sigintEvent->add();
		$sighupEvent = Event::signal($this->base, SIGHUP, function ($no, $c) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGHUP]);
		});
		$sighupEvent->add();

		$n = 0.5;
		$flags = Event::TIMEOUT | Event::PERSIST;
		$event = new Event($this->base, -1, $flags, function ($n) use (&$processes) {
			/** @var ChildProcess $process */
			foreach ($processes as $key => $process) {
				$status = $process->getStatus();

				if (!$status['running']) {
					$this->eventWorkerFinish($status['exitcode'], $process->getPID(), $processes);
				}
			}
		});
		$event->add($n);

		$this->base->loop();

		return $this->fails;
	}
}
<?php
namespace TkachInc\ChildProcesses\Handlers;
use TkachInc\ChildProcesses\EventHandler;

/**
 * @author maxim
 */
class EvChildProcessHandler extends AbstractProcessHandler
{
	public function init()
	{
		$this->eventHandler->on(EventHandler::EVENT_CHECK_COMPLETE, function () {
			\Ev::stop(\Ev::BREAK_ALL);
		});
	}

	public function check(&$processes)
	{
		$this->eventHandler->emit(EventHandler::EVENT_CHECK_INIT, []);
		// Required create variable (to allocate memory), otherwise it will not been seen Ev::run
		$evSingnalTerm = new \EvSignal(SIGTERM, function ($watcher) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGTERM]);
		});
		$evSingnalInt = new \EvSignal(SIGINT, function ($watcher) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGINT]);
		});
		$evSingnalHup = new \EvSignal(SIGHUP, function ($watcher) {
			$this->eventHandler->emit(EventHandler::EVENT_STATUS_CLOSE, [SIGHUP]);
		});

		// Required create variable (to allocate memory), otherwise it will not been seen Ev::run
		$evChild = new \EvChild(0, true, function (\EvChild $child) use (&$processes) {
			$this->eventWorkerFinish($child->rstatus, $child->rpid, $processes);
		});

		\Ev::run();

		return $this->fails;
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 4/6/17
 * Time: 14:01
 */

namespace TkachInc\ChildProcesses\Handlers;

use Evenement\EventEmitter;
use TkachInc\ChildProcesses\ChildProcess;
use TkachInc\ChildProcesses\EventHandler;

abstract class AbstractProcessHandler implements InterfaceProcessHandler
{
	protected $fails = [];

	protected $eventHandler;

	public function __construct(EventHandler $eventHandler)
	{
		$this->eventHandler = $eventHandler;
		$this->eventHandler->on(EventHandler::EVENT_CHECK_INIT, function () {
			$this->fails = [];
		});

		$this->init();
	}

	public function getEventEmitter() {
		return $this->eventHandler;
	}

	protected function eventWorkerFinish($status, $pid, &$processes)
	{
		/** @var ChildProcess $process */
		$process = $processes[$pid];

		if ($status !== 0) {
			$this->fails[] = $process;
		}

		$process->close();

		unset($processes[$pid]);

		$this->eventHandler->emit(EventHandler::EVENT_WORKER_FINISH, [$process, $status]);
		if (empty($processes)) {
			$this->eventHandler->emit(EventHandler::EVENT_CHECK_COMPLETE, [$this->fails]);
		}
	}
}
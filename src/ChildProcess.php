<?php
namespace TkachInc\ChildProcesses;

/**
 * @author maxim
 */
class ChildProcess
{

	/**
	 * @var string child process command
	 */
	protected $command;

	/**
	 * @var resource child process resource
	 */
	protected $resource;

	/**
	 * @var mixed
	 */
	protected $data;

	/**
	 * @var int child process pid
	 */
	protected $pid = 0;

	/**
	 * @var resource
	 */
	protected $stdin;

	/**
	 * @var resource
	 */
	protected $stdout;

	/**
	 * @var resource
	 */
	protected $stderr;

	/**
	 * @var
	 */
	protected $pipes;

	/**
	 * @var bool
	 */
	protected $killing = false;

	/**
	 * Process constructor.
	 *
	 * @param string $command command
	 * @param null $data
	 * @param resource $stdin
	 * @param resource $stdout
	 * @param resource $stderr
	 *
	 * @throws \Exception
	 */
	public function __construct($command, $data = null, $stdin = STDIN, $stdout = STDOUT, $stderr = STDERR)
	{
		$this->command = $command;

		$this->data = $data;

		$this->stdin = $stdin;

		$this->stdout = $stdout;

		$this->stderr = $stderr;

		$this->start();
	}

	public function start()
	{
		if (!is_resource($this->resource) || !$this->isRunning()) {
			$this->resource = proc_open('exec '.$this->command, [$this->stdin, $this->stdout, $this->stderr], $this->pipes);

			if (!$this->resource) {
				throw new \Exception('Process not start', 127);
			}

			$status = $this->getStatus();

			if (!isset($status['pid'])) {
				throw new \Exception('Not found pid', 128);
			}
			$this->pid = $status['pid'];
		}
	}

	/**
	 * Run command and create new ChildProcess
	 *
	 * @param string $command
	 * @param null $data
	 * @param resource $stdin
	 * @param resource $stdout
	 * @param resource $stderr
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public static function run($command, $data = null, $stdin = STDIN, $stdout = STDOUT, $stderr = STDERR)
	{
		return (new static($command, $data, $stdin, $stdout, $stderr));
	}

	/**
	 * Return execute command
	 *
	 * @return string
	 */
	public function getCommand()
	{
		return $this->command;
	}

	/**
	 * Return child process resource
	 *
	 * @return resource
	 */
	public function getResource()
	{
		return $this->resource;
	}

	/**
	 * @return resource
	 */
	public function getStdIn()
	{
		return $this->stdin;
	}

	/**
	 * @return resource
	 */
	public function getStdOut()
	{
		return $this->stdout;
	}

	/**
	 * @return resource
	 */
	public function getStdErr()
	{
		return $this->stderr;
	}

	/**
	 * @return mixed
	 */
	public function getPipes()
	{
		return $this->pipes;
	}

	/**
	 * Return current process status
	 *
	 * @return array
	 */
	public function getStatus()
	{
		return proc_get_status($this->resource);
	}

	/**
	 * @return bool|mixed
	 */
	public function isRunning()
	{
		$status = $this->getStatus();

		return $status['running']??false;
	}

	/**
	 * Return current process pid
	 *
	 * @return mixed
	 */
	public function getPID()
	{
		return $this->pid;
	}

	/**
	 * @return mixed|null
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Terminate child process (close parent sh process)
	 *
	 * @param null|int $signal
	 *
	 * @return bool
	 */
	public function terminate($signal = null)
	{
		return proc_terminate($this->resource, $signal);
	}

	/**
	 * Close all process by command
	 *
	 * @param $signal
	 *
	 * @return string
	 */
	public function kill($signal = SIGTERM)
	{
		$this->killing = true;
		return posix_kill($this->getPID(), $signal);
	}

	/**
	 * Close current process
	 */
	public function close()
	{
		$this->killing = true;
		return proc_close($this->resource);
	}

	/**
	 * @return bool
	 */
	public function isKilling() {
		return $this->killing;
	}
}
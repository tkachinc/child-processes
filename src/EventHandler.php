<?php
namespace TkachInc\ChildProcesses;
use Evenement\EventEmitter;

/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 4/7/17
 * Time: 18:19
 */
class EventHandler extends EventEmitter
{
	const EVENT_STATUS_CLOSE = 'event.status.close';

	const EVENT_ERROR_CATCH = 'event.error.catch';

	const EVENT_WORKER_FINISH = 'event.worker.close';

	const EVENT_CHECK_COMPLETE = 'event.check.complete';

	const EVENT_CHECK_INIT = 'event.check.init';

	const TYPE_ERROR = 'ERROR';

	const TYPE_THROWABLE = 'THROWABLE';

	const TYPE_LAST_ERROR = 'LAST_ERROR';

	public function __construct() {
		set_error_handler([$this, 'errorHandler']);
		set_exception_handler([$this, 'exceptionHandler']);
		register_shutdown_function([$this, 'shutdownHandler']);
	}

	/**
	 * @param $error_number
	 * @param $error_string
	 * @param $error_file
	 * @param $error_line
	 */
	public function errorHandler($error_number, $error_string, $error_file, $error_line)
	{
		$response = [
			'number' => $error_number,
			'string' => $error_string,
			'file' => $error_file,
			'line' => $error_line,
		];
		$response['type'] = self::TYPE_ERROR;

		$this->emit(self::EVENT_ERROR_CATCH, [$response]);
	}

	/**
	 * @param \Throwable $e
	 */
	public function exceptionHandler(\Throwable $e)
	{
		$response = [
			'code' => (int)$e->getCode(),
			'message' => (string)$e->getMessage(),
			'file' => (string)$e->getFile(),
			'line' => (int)$e->getLine(),
		];
		$response['type'] = self::TYPE_THROWABLE;

		$this->emit(self::EVENT_ERROR_CATCH, [$response]);
	}

	/**
	 * Shutdown error handler
	 */
	public function shutdownHandler()
	{
		$e = error_get_last();
		if ($e !== null) {
			$response['type'] = self::TYPE_LAST_ERROR;
			$response['message'] = $e;

			$this->emit(self::EVENT_ERROR_CATCH, [$response]);
		}
	}
}
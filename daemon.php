<?php
/**
 * @author maxim
 */
include('vendor/autoload.php');

$child = new TkachInc\ChildProcesses\ChildProcesses(new \TkachInc\ChildProcesses\EventHandler());
$child->add('php worker.php');
$child->daemon();
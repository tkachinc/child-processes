[![Codacy Badge](https://api.codacy.com/project/badge/Grade/844766ff73c0494a975121b6f7c1db1f)](https://www.codacy.com/app/gollariel/child-processes?utm_source=tkachinc@bitbucket.org&amp;utm_medium=referral&amp;utm_content=tkachinc/child-processes&amp;utm_campaign=Badge_Grade)

# Child Processes

This library is designed to simple manipulation with child processes.
To check exit status use Ev or Event or simple loop. And not need any library!

## Example for use

**Wait success complete**

```php
$event = EventHandler::getInstance();
$child = new ChildProcesses($event);
$child->add('<system command>');
$child->add('<system command>');
$child->add('<system command>');
$child->add('<system command>');
$child->add('<system command>');
$child->getHandlerEmitter()->on(AbstractProcessHandler::EVENT_CHECK_COMPLETE, function() {
    if (empty($fails)) {
        // TODO Complete generation
    } else {
        // TODO Fail scope
    }
});
$fails = $child->check();
```

**Example wait daemon process**

Your parent.php
```php
$event = EventHandler::getInstance();
$child = new ChildProcesses($event);
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w1'));
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w2'));
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w3'));
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w4'));
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w5'));
$child->addProcessInstance(new \TkachInc\ChildProcesses\ChildProcess('php worker.php -w6'));
$child->daemon();
```

Your worker.php
```php
$options = getopt("w:");
if(isset($options['w']))
{
    echo $options['w'].PHP_EOL;

    switch ($options['w'])
    {
        case "1":
            sleep(5);
            break;
        case "2":
            sleep(5);
            break;
        case "3":
            sleep(7);
            break;
        case "4":
            sleep(5);
            break;
        case "5":
            sleep(7);
            break;
        case "6":
            sleep(5);
            break;
    }
}
```
